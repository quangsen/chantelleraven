<?php $mts_options = get_option('magxp'); ?>
<?php get_header(); ?>
<div class="main-container">
	<div id="page" class="single">
		<article class="<?php echo $article_class; ?>">
			<div id="content_box" >
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<div id="post-<?php the_ID(); ?>" <?php post_class('g post'); ?>>
						<div class="single_page">
							<header>
								<h1 class="title"><?php the_title(); ?></h1>
							</header>
							<div class="post-content box mark-links">
								<?php the_content(); ?>
							</div><!--.post-content box mark-links-->
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</article>
    </div>
</div>
<?php get_footer(); ?>