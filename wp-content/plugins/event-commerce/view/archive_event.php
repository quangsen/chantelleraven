
<?php 
    get_header(); 
?> 
    <div id="page-title" class="page-title-block page-title-alignment-center page-title-style-1 ">
        <div class="container">
            <div class="page-title-title">
                <h1>Events</h1>
            </div>
        </div>
        <div class="breadcrumbs-container">
            <div class="container">
                <div class="breadcrumbs">
                    <span>
                        <a href="<?php echo( get_home_url ); ?>" itemprop="url">
                            <span itemprop="title">Home</span>
                        </a>
                    </span> 
                    <span class="divider">
                        <span class="bc-devider"></span>
                    </span> 
                    <span class="current">Events</span>
                </div><!-- .breadcrumbs -->
            </div>
        </div>
    </div>
    <div class="event-container">
    <?php
        $events = tribe_get_events( array(
            'posts_per_page' => 9
        ) );

        foreach( $events as $event ) {
    ?>
            <div class="event-item clearfix">
                <div class="event-item-left-col" >
                    <a href="<?php echo( $event->guid ); ?>">
                        <img src="<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>" alt="" />
                    </a>
                    <div class="even-item-time">
                        <p class="month"><?php echo( tribe_get_start_date( $event, false, $format = 'M' ) ); ?></p>
                        <p class="day"><?php echo( tribe_get_start_date( $event, false, $format = 'j' ) ); ?></p>
                        <p class="hours"><?php echo( tribe_get_start_date( $event, false, $format = 'g: i A' ) ); ?></p>
                    </div>
                </div>
                <div class="event-item-right-col">
                    <a href="<?php echo( $event->guid ); ?>">
                        <h1><?php echo( $event->post_title ); ?></h1>
                    </a>
                    <div class="event-item-location">
                        <p><?php echo( tribe_get_start_date( $event, false, $format = 'l, M j, Y' ) ); ?></p>
                        <p>
                            <?php echo( 
                                    tribe_get_start_date( $event, false, $format = 'G:i A' ) . ' - ' . tribe_get_end_date( $event, false, $format = 'G:i A' ) 
                                ); 
                            ?>
                        </p>
                        <p>
                            <?php 
                                echo( 
                                    trim( tribe_get_venue( $event->ID ) ) != '' ? 
                                    tribe_get_venue( $event->ID ) . '<a href="' . tribe_get_map_link( $event->ID ) . '"> ( map )</a>' : 
                                    'Comming Soon!!!' 
                                ); 
                            ?>
                        </p>
                    </div>
                    <p>
                    <?php echo( wp_strip_all_tags( substr( $event->post_content, 0, 100 ) . '...' ) ); ?>
                    </p>
                </div>
            </div>
    <?php
        }
    ?>
    </div>

<?php 
    get_footer(); 
?>