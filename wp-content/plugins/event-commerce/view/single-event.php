<?php
	global $post;
	$event = $post;
	$product = wc_get_product( $post->ID );
	$earlier_event = tribe_get_events( array(
	 	'posts_per_page' => 5,
	 	'post__not_in' => array( $event->ID ),
	 	'end_date' => tribe_get_start_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	 	'orderby' => 'DATE',
	 	'order' => 'DESC'
	) );
	$later_event = tribe_get_events( array(
	 	'posts_per_page' => 1,
	 	'post__not_in' => array( $event->ID ),
	 	'start_date' => tribe_get_end_date( $event, true, $format = 'yyyy-mm-dd hh:mm' ),
	 	'orderby' => 'DATE',
	 	'order' => 'DESC'
	) );

	get_header();

	$post = $event;
?>
	<div class="event-single">
		<div 
			class="event-single-top-container" 
			style="background: url( '<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>' );">
		</div>
		<div class="event-single-bottom-container">
			<div class="event-single-description">
				<h1><?php echo( $event->post_title ); ?></h1>
				<p><?php echo( $event->post_content ) ?></p>
				<div class="form-container">
					<?php 
						$field = get_field_object('form_survey');
						$value = $field['value'];
						$label = $field['choices'][ $value ];
						$form_id = str_replace( 'formsurvey', '', $value );
					    echo do_shortcode('[gravityform id="'. $form_id .'" title="false" description="false" ajax="true"]');
					?>
						<a class="sign-up-btn" href="#ngaw">SECURE YOUR PLACE</a>
					<?php
						do_action( 'woocommerce_single_product_summary' );
					?>
				</div>
			</div>
			<div class="event-single-location">
				<ul>
					<li>
						<h6>Date </h6>
						<h4><?php echo( tribe_get_start_date( $event, false, $format = 'F d, Y' ) ); ?></h4>
						<p>
							<?php echo( tribe_get_start_date( $event, false, $format = 'D' ) . ' until ' . tribe_get_end_date( $event, false, $format = 'F d, Y' ) ); 
							?>
						</p>
					</li>
					<li>
						<h6>Time </h6>
						<h4>
							<?php echo( tribe_get_start_date( $event, false, $format = 'G:i a' ) . ' - ' . tribe_get_end_date( $event, false, $format = 'G:i a' ) ); ?>
						</h4>
					</li>
					<li>
						<h6>Location </h6>
						<h4>
						<?php 
							echo( 
								trim( tribe_get_venue( $event ) ) != '' ?
								tribe_get_venue( $event ) : 'Comming Soon!!!'
							); 
						?>
						</h4>
						<p>
						<?php
							echo(
								trim( tribe_get_venue( $event ) ) != '' ?
								'at ' . tribe_get_address( $event ) : ''
							);
						?>	
						</p>
					</li>
				</ul>
			</div>
			<div class="button-container">
				<?php
					if( count( $earlier_event ) > 0 ) {
						echo( '
							<div class="prev-btn">
								<a href="' . $earlier_event[0]->guid . '">
									<i class="fa fa-long-arrow-left" aria-hidden="true"></i> EALIER( ' . 
										tribe_get_start_date( $earlier_event[0], false, $format = 'G:i A' ) .
										' '.
										$earlier_event[0]->post_title .
									' )
								</a>
							</div>
						' );
					}

					if( count( $later_event ) > 0 ) {
						echo( '
							<div class="next-btn">
								<a href="' . $later_event[0]->guid . '">
									LATER( ' . 
										tribe_get_start_date( $later_event[0], false, $format = 'G:i A' ) .
										' '.
										$later_event[0]->post_title .
									' )
									<i class="fa fa-long-arrow-right" aria-hidden="true"></i> 
								</a>
							</div>
						' );
					}
				?>
			</div>
		</div>
	</div>
	<div class="past-event event-container">
		<?php
			if( count( $earlier_event ) > 0 ) {
                echo( '<h1>5 RECENT PAST EVENTS</h1>' );
            } else {
                echo( '<h1>THERE IS NO PAST EVENTS TO SHOW</h1>' );
            }
			foreach( $earlier_event as $event ) {
	    ?>
	            <div class="event-item clearfix">
	                <div class="event-item-left-col" >
	                    <a href="<?php echo( $event->guid ); ?>">
	                        <img src="<?php echo( get_the_post_thumbnail_url( $event->ID, 'full' ) ); ?>" alt="" />
	                    </a>
	                    <div class="even-item-time past">
	                        <p class="month"><?php echo( tribe_get_start_date( $event, false, $format = 'M' ) ); ?></p>
	                        <p class="day"><?php echo( tribe_get_start_date( $event, false, $format = 'j' ) ); ?></p>
	                        <p class="hours"><?php echo( tribe_get_start_date( $event, false, $format = 'g: i A' ) ); ?></p>
	                    </div>
	                </div>
	                <div class="event-item-right-col">
	                    <a href="<?php echo( $event->guid ); ?>">
	                        <h1><?php echo( $event->post_title ); ?></h1>
	                    </a>
	                    <div class="event-item-location">
	                        <p><?php echo( tribe_get_start_date( $event, false, $format = 'l, M j, Y' ) ); ?></p>
	                        <p>
	                            <?php echo( 
	                                    tribe_get_start_date( $event, false, $format = 'G:i A' ) . ' - ' . tribe_get_end_date( $event, false, $format = 'G:i A' ) 
	                                ); 
	                            ?>
	                        </p>
	                        <p>
	                            <?php 
	                                echo( 
	                                    trim( tribe_get_venue( $event->ID ) ) != '' ? 
	                                    tribe_get_venue( $event->ID ) . '<a href="' . tribe_get_map_link( $event->ID ) . '"> ( map )</a>' : 
	                                    'Comming Soon!!!' 
	                                ); 
	                            ?>
	                        </p>
	                    </div>
	                    <p>
	                    <?php echo( wp_strip_all_tags( substr( $event->post_content, 0, 100 ) . '...' ) ); ?>
	                    </p>
	                </div>
	            </div>
	    <?php
	        }
	    ?>
	</div>
<?php
	get_footer();